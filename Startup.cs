﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace Abstimmung
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>();


			services.AddIdentity<User, IdentityRole>()
					.AddEntityFrameworkStores<DataContext>()
					.AddDefaultTokenProviders();


			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = true;
				options.Password.RequiredLength = 4;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;

				options.Lockout.AllowedForNewUsers = true;
				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(600);
				options.Lockout.MaxFailedAccessAttempts = 5;

				options.User.RequireUniqueEmail = false;

				options.SignIn.RequireConfirmedEmail = false;
				options.SignIn.RequireConfirmedPhoneNumber = false;
			});
            
			services.AddMvc(options =>
			{
				options.OutputFormatters.Clear();
				options.OutputFormatters.Add(new JsonOutputFormatter(new JsonSerializerSettings()
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				}, ArrayPool<char>.Shared));
			}).AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
			

			services.ConfigureApplicationCookie(options =>
			{
				options.LoginPath = "/Login";
				options.LogoutPath = "/Login";
				options.AccessDeniedPath = "/Login";
				options.SlidingExpiration = true;
				options.Cookie = new CookieBuilder
				{
					Expiration = TimeSpan.FromMinutes(600),
					HttpOnly = true,
					Name = "AuthorizeCookie",
					Path = "/",
					SameSite = SameSiteMode.Lax,
					SecurePolicy = CookieSecurePolicy.SameAsRequest
				};
			});
			services.AddSession(options => {
				options.IdleTimeout = TimeSpan.FromMinutes(600);
			});
			services.ConfigureSwaggerGen(options =>
			{
				options.CustomSchemaIds(x => x.FullName);
			});
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "Housekeeping System API", Version = "v1" });
			});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
			app.UseStaticFiles(new StaticFileOptions());
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Contacts API V1");
            });
            app.UseAuthentication();
		    CreateRoles(serviceProvider);
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Login}/{action}");
			});
        }
        private void CreateRoles(IServiceProvider serviceProvider)
		{
			var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
			Task<User> admin = userManager.FindByNameAsync("Admin");
			admin.Wait();
			if (admin.Result == null)
			{
				User Admin = new User();
				Admin.UserName = "Admin";
				var res = userManager.CreateAsync(Admin, "1234");
				res.Wait();
				var x = res;
			}
		}
    }
}
