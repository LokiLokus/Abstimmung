app.controller('VoteCtrl', function ($scope, $http, $timeout) {
    $scope.con = {};
    $scope.newVote = null;

    $scope.NewVote = function(){
        $scope.newVote = {
            Question:"",
            ConferenceID:urlParam("id"),
            Answers:[""]
        };
    }

    $scope.SaveVotes = function(){
        $('#SaveVotes').addClass('progress-qk');
        for (let i = 0; i < $scope.con.Votes.length; i++) {
            const element = $scope.con.Votes[i];
            if($('input[name=' + (i+1) + ']:checked').val() != undefined){
                element.SelectedAnswer = $scope.con.Votes[i].Answers[$('input[name=' + (i+1) + ']:checked').val()];
            }
        }
        $http({
            method: 'PUT',
            url: '/api/Vote/AnswerVote',
            data: $scope.con
        }).then(function mySuccess(response) {
            console.log("HALLO2");
            $('#SaveVotes').removeClass('progress-qk');
            $scope.LoadCon();
        }, function myError(response) {
            $('#SaveVotes').removeClass('progress-qk');
            $scope.LoadCon();
            httpError();
        });
    }

    $scope.SaveVote = function(){
        $('#SaveVote').addClass('progress-qk');
        $http({
            method: 'POST',
            url: '/api/Vote/AddVoteToConference/' + urlParam("id"),
            data: $scope.newVote
        }).then(function mySuccess(response) {
            $('#SaveVote').removeClass('progress-qk');
            $scope.newVote = null;
            $scope.LoadCon();
        }, function myError(response) {
            $('#SaveVote').removeClass('progress-qk');
            httpError();
        });
    }

    $scope.UpdateVoteAnswer = function(){
        var tmp = $.grep($scope.newVote.Answers,function(e){
            return e.trim().length == 0;
        });
        if(tmp.length < 1){
            $scope.newVote.Answers.push("");

        }
        console.log($scope.newVote)
        console.log($scope.newVote.Answers[$scope.newVote.Answers.length-1].length)
    }

    $scope.LoadCon = function () {
        $http({
            method: 'GET',
            url: '/api/Vote/GetConference/' + urlParam("id")
        }).then(function mySuccess(response) {
            console.log(response.data);
            $scope.con = response.data;
            
        }, function myError(response) {
            httpError();
        });
    }
    $scope.LoadCon();
});