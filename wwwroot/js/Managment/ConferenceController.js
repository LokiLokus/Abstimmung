app.controller('ConCtrl', function ($scope, $http, $timeout) {
    $scope.confernces = [];
    $scope.selItem = null;
    $scope.Statisitc = [];


    $scope.LoadConfernces = function () {
        $http({
            method: 'GET',
            url: '/api/Conference/GetAllConfernces'
        }).then(function mySuccess(response) {
            console.log(response.data)
            for (let i = 0; i < response.data.length; i++) {
                const elm = response.data[i];
                elm.Time = DateTimeToTime(elm.Timestamp)
            }
            if($scope.selItem != null && $scope.selItem.ID == undefined){
                $scope.selItem = $.grep(response.data,function(e){
                    e.ID == $scope.selItem.ID
                })[0];
            }
            $scope.confernces = response.data;
        }, function myError(response) {
            httpError();
        });
    }

    $scope.RecalcTime = function(){

        var date = $('.datepicker').datepicker('getDate');
        date.setHours(GetHoursFromTime($scope.selItem.Time));
        date.setMinutes(GetMinutesFromTime($scope.selItem.Time));
        $scope.selItem.Timestamp = date;
        console.log($scope.selItem.Timestamp)
    }

    $scope.Save = function () {
        $('#save').addClass('progress-qk');
        if ($scope.selItem.ID == undefined) {
            //CREATE
            $http({
                method: 'POST',
                url: '/api/Conference/CreateConfernce',
                data: $scope.selItem
            }).then(function mySuccess(response) {
                $('#save').removeClass('progress-qk');
                console.log(response.data)
                if(response.data.error != undefined){
                    alert("Fehler aufgetreten bitte nochmal versuchen " + response.data.error);
                }else{
                    $scope.selItem.ID = response.data.success
                }
                $scope.LoadConfernces();
                
            }, function myError(response) {
                $('#save').removeClass('progress-qk');
                httpError();
            });
        } else {
            //UPDATE
            $http({
                method: 'PUT',
                url: '/api/Conference/UpdateConfernce/' + $scope.selItem.ID,
                data:$scope.selItem
            }).then(function mySuccess(response) {
                $('#save').removeClass('progress-qk');
                console.log(response.data)
                if(response.data.error != undefined){
                    alert("Fehler aufgetreten bitte nochmal versuchen " + response.data.error);
                }
                $scope.LoadConfernces();
            }, function myError(response) {
                $('#save').removeClass('progress-qk');
                httpError();
            });
        }
    }

    $scope.New = function(){
        var tmp = new Date();
        tmp.setHours(12);
        tmp.setMinutes(0);
        $scope.SelectItem({
            Name: "",
            Timestamp: tmp,
            Time: '12:00'
        });
        $scope.Statisitc = [];
    }

    $scope.LoadStatistic = function(){
        $http({
            method: 'GET',
            url: '/api/Vote/GetStatistics/' + $scope.selItem.ID
        }).then(function mySuccess(response) {
            $('#save').removeClass('progress-qk');
            console.log(response.data);
            $scope.Statisitc = response.data;            
        }, function myError(response) {
            httpError();
        });
    }

    $scope.SelectItem = function (item) {
        $scope.selItem = item;
        $scope.LoadStatistic();

        $timeout(function () {
            $('.clockpicker').clockpicker({
                autoclose: true
            });
            $('.datepicker').datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                    'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                showMonthAfterYear: false,
                showOn: 'both',
                buttonImage: 'media/img/calendar.png',
                buttonImageOnly: true,
                dateFormat: 'd MM, yy'
            }
            );
            $('.datepicker').datepicker('setDate', new Date($scope.selItem.Timestamp));
        }, 100);
    }
    $scope.LoadConfernces();
    setInterval(function () {
        $('.ui-datepicker-trigger').hide();
    }, 100);
});