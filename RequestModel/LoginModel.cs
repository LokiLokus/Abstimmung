using System.ComponentModel.DataAnnotations;

namespace Abstimmung.RequestModel
{
    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}