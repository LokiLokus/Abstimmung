using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;

namespace Abstimmung.RequestModel
{
    public class ConferenceModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }

        internal Conference ToConference(User user)
        {
            Conference result = new Conference()
            {
                CreatorID = user.Id,
                CreatedAt = DateTime.UtcNow,
                Name = this.Name,
                Timestamp = this.Timestamp
            };
            return result;
        }
    }

    public class ConferenceVoteModel : ConferenceModel
    {
        public List<VoteModel> Votes { get; set; }

        public static ConferenceVoteModel ToConferenceVoteModel(Conference con, string userID, DataContext cxt)
        {
            ConferenceVoteModel result = new ConferenceVoteModel()
            {
                ID = con.ID,
                Name = con.Name,
                Timestamp = con.Timestamp
            };

            List<VoteModel> votes = cxt.Vote.Where(x => x.ConferenceID == con.ID)
                .Select(x => new VoteModel()
                {
                    Answers = x.Answers,
                    ID = x.ID,
                    Question = x.Question,
                    ConferenceID = con.ID
                }).ToList();
            votes.ForEach(x =>
            {
                UserVote tmp = cxt.UserVote.SingleOrDefault(y => y.VoteID == x.ID && y.UserID == userID);
                if (tmp != null)
                {
                    x.Answerd = true;
                    x.SelectedAnswer = tmp.Answer;
                }
            });
            result.Votes = votes;

            return result;
        }
    }

    public class VoteModel
    {
        public int ID { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public List<string> Answers { get; set; }
        public int ConferenceID { get; set; }
        public string SelectedAnswer { get; set; }
        public bool Answerd { get; set; }

        public Vote ToVote(int confID)
        {
            Vote result = new Vote()
            {
                Question = Question,
                Answers = Answers,
                ConferenceID = confID
            };
            return result;
        }
    }

    public class StatisticModel
    {
        public string Question { get; set; }
        public List<Dictionary<string, int>> Answers { get; set; }
    }
}