using System.Collections.Generic;

namespace Abstimmung.RequestModel
{
     public class RequestResult : Dictionary<string,string>
    {
        public static RequestResult GetRequestResult(bool error, string message){
            var result = new RequestResult();
            if(error){
                result.Add("error",message);
            }else{
                result.Add("success",message);
            }
            return result;
        }


        public static RequestResult ModelStateInvalid()
        {
            var result = new RequestResult();
            result.Add("error", "Allgemeiner Fehler bitte versuchen Sie es erneut");
            return result;
        }


        public static RequestResult ObjectNotFound()
        {
            var result = new RequestResult();
            result.Add("error", "Die angegebene Eigenschaft konnte nicht gefunden werden. Bitte versuchen Sie es erneut");
            return result;
        }
    }
}