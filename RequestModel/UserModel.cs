using Abstimmung.DBModel.Model;

namespace Abstimmung.RequestModel
{
    public class UserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public static UserModel ToUserModel(User user){
            if(user == null){
                return null;
            }
            return new UserModel(){
                Id = user.Id,
                UserName = user.UserName
            };
        }
    }
}