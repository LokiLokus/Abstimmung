using Abstimmung.DBModel.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Abstimmung.DBModel
{
    public class DataContext : IdentityDbContext<User, IdentityRole, string>
	{
		public DataContext(){}
        
        public DbSet<Conference> Conference { get; set; }
        public DbSet<Vote> Vote { get; set; }
        public DbSet<UserVote> UserVote { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=hummli.de;Persist Security Info=True;database=Loki_Abstimmung;uid=Abstimmung_User;pwd=abstimmungzt3gajsd;");
        }
    }   
}