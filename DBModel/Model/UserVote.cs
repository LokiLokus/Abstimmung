using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Abstimmung.DBModel.Model
{
    public class UserVote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [ForeignKey("User")]
        public string UserID { get; set; }
        [ForeignKey("Vote")]
        public int VoteID { get; set; }
        [Required]
        public string Answer { get; set; }
        public DateTime Timestamp { get; set; }

        public virtual User User { get; set; }
        public virtual Vote Vote { get; set; }
    }
}