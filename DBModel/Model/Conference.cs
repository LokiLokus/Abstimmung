using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abstimmung.RequestModel;
using Newtonsoft.Json;

namespace Abstimmung.DBModel.Model
{
    public class Conference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public DateTime Timestamp { get; set; }
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public string Name { get; set; }

        [ForeignKey("Creator")]
        public string CreatorID { get; set; }

        public UserModel CreatedBy { get{
            return UserModel.ToUserModel(Creator);
        }}
        [JsonIgnore]
        public User Creator { get; set; }
    }
}