using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Abstimmung.DBModel.Model
{
    public class Vote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        public string Question { get; set; }
        
        public string _answer { get; set; }
        [NotMapped]
        public List<string> Answers { get{
            if(_answer == null)
                return new List<string>();
            return JsonConvert.DeserializeObject<List<string>>(_answer);
        } set{
            _answer = JsonConvert.SerializeObject(value);
        } }

        [ForeignKey("Conference")]
        public int ConferenceID { get; set; }

        public virtual Conference Conference { get; set; }
    }
}