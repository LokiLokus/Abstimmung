using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Abstimmung.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Abstimmung.Controllers
{
    [Authorize]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly DataContext dbContext;

        public UserController(UserManager<User> userManager, SignInManager<User> signInManager, DataContext dbContext)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
        }

        [HttpGet("GetUser")]
        public List<Dictionary<string,string>> GetUser(){
            List<Dictionary<string,string>> result = new List<Dictionary<string,string>>();
            
            dbContext.Users.ToList().ForEach(x => {
                var tmp = new Dictionary<string,string>();
                tmp.Add("UserName",x.UserName);
                tmp.Add("ID",x.Id);
                result.Add(tmp);
            });
            return result;
        }

        [HttpPost("CreateUser")]
        public async Task<RequestResult> CreateUserAsync([FromBody]UserModel model){
            User user = new User(){
                UserName = model.UserName
            };
            var result = await userManager.CreateAsync(user,model.Password);
            if(result.Succeeded){
                return RequestResult.GetRequestResult(false,"Erstellt");
            }else{
                return RequestResult.GetRequestResult(true,"Fehler beim erstellen des Benutzters prüfen sie den UserName und das Passwort");
            }
        }

        [HttpDelete("DeleteUser/{id}")]
        public async Task<RequestResult> DeleteUserAsync(string id){
            await userManager.DeleteAsync(await userManager.FindByIdAsync(id));
            return RequestResult.GetRequestResult(false,"Gelöscht");
        }
    }
}