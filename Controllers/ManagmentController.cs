using System.Linq;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Abstimmung.Controllers
{
    [Authorize]
    [Route("Managment")]
    public class ManagmentController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly DataContext dbContext;

        public ManagmentController(UserManager<User> userManager, SignInManager<User> signInManager, DataContext dbContext)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("User")]
        public IActionResult User()
        {
            return View();
        }

        [HttpGet("Confernces")]
        public IActionResult Confernces()
        {
            ViewData["Confernces"] = dbContext.Conference.Include(x => x.Creator).ToList();

            return View();
        }
        [HttpGet("Vote")]
        public IActionResult Vote()
        {
            return View();
        }
    }
}