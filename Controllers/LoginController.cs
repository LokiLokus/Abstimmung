using System.Threading.Tasks;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Abstimmung.RequestModel;
using Abstimmung.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Abstimmung.Controllers
{
    [Route("Login")]
    public class LoginController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly DataContext dbContext;

        public LoginController(UserManager<User> userManager,SignInManager<User> signInManager,DataContext dbContext){
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index(){
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Index(LoginModel model){
            if(ModelState.IsValid){
                LoginResult loginresult = await AuthenticationService.LoginUserAsync(model);
                if(loginresult == LoginResult.Succeeded){
                    return Redirect("/Managment");
                }else if(loginresult == LoginResult.UserNotExist){
                    ViewData["Error"] = "Benutzer existiert nicht";
                }else if(loginresult == LoginResult.PasswordWrong){
                    ViewData["Error"] = "Passwort ist nicht korrekt";
                }
            }else{
                ViewData["Error"] = "Fehler ist aufgetreten. Bitte versuchen Sie es erneut";
            }
            return PartialView();
        }

    }
}