using System.Linq;
using System.Threading.Tasks;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Abstimmung.RequestModel;

namespace Abstimmung.Services
{
    public enum LoginResult
    {
        Succeeded,
        UserNotExist,
        PasswordWrong
    }

    
    public static class AuthenticationService
    {
        ///<summary>
        /// Logins User if correct returns 0
        /// User doesn't exists 1
        /// Password incorrect 2
        ///</summary>
        public static async Task<LoginResult> LoginUserAsync(LoginModel model){
            DataContext dbContext = ServiceProviderFactory.GetDataContext();
            //Check User exists per Database and not UserManager cause supports multithread
            User user = dbContext.Users.SingleOrDefault(x => x.UserName == model.UserName);
            if(user == null){
                return LoginResult.UserNotExist;
            }
            var result = await ServiceProviderFactory.GetSignInManager().PasswordSignInAsync(user,model.Password,false,false);
            if(result.Succeeded){
                return LoginResult.Succeeded;
            }else{
                return LoginResult.PasswordWrong;
            }
        }
    }
}