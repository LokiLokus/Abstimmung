using System;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Abstimmung.Services
{
    public static class ServiceProviderFactory
    {
        private static IServiceProvider ServiceProvider { get; }
        
        static ServiceProviderFactory()
        {
            IStartup startup = Program.Startup;
            ServiceCollection sc = new ServiceCollection();
            startup.ConfigureServices(sc);
            ServiceProvider = sc.BuildServiceProvider();
        }
        
        internal static DataContext GetDataContext(){
            return new DataContext();
        }

        internal static SignInManager<User> GetSignInManager(){
            return ServiceProvider.GetService<SignInManager<User>>();
        }

        internal static UserManager<User> GetUserManager()
        {
            return ServiceProvider.GetService<UserManager<User>>();
        }
    }
}