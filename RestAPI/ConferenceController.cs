using System;
using System.Collections.Generic;
using System.Linq;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Abstimmung.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Abstimmung.RestAPI
{
    [Authorize]
    [Route("api/Conference")]
    public class ConferenceController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly DataContext dbContext;

        public ConferenceController(UserManager<User> userManager,SignInManager<User> signInManager,DataContext dbContext){
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
        }

        [HttpGet("GetNextConfernces")]
        public List<Conference> GetNextConfernces(){
            return dbContext.Conference.Where(x => x.Timestamp >= DateTime.UtcNow.AddDays(-1)).OrderBy(x => x.Timestamp)
                .Include(x => x.Creator).ToList();
        }

        [HttpGet("GetAllConfernces")]
        public List<Conference> GetAllConfernces(){
            return dbContext.Conference.OrderBy(x => x.Timestamp)
                .Include(x => x.Creator).ToList();
        }

        [HttpPost("CreateConfernce")]
        public RequestResult CreateConfernce([FromBody]ConferenceModel model){
            if(ModelState.IsValid){
                User user = dbContext.Users.SingleOrDefault(x => x.UserName == User.Identity.Name);
                if(dbContext.Conference.SingleOrDefault(x => x.Name == model.Name && x.Timestamp.Date == model.Timestamp.Date) == null){
                    Conference tmp = model.ToConference(user);
                    dbContext.Conference.Add(tmp);
                    dbContext.SaveChanges();
                    return RequestResult.GetRequestResult(false,tmp.ID + "");
                }else{
                    return RequestResult.GetRequestResult(true,"Konferenz mit selben Namen am selben Tag gibt es bereits");
                }
            }else{
                return RequestResult.ModelStateInvalid();
            }
        }

        [HttpPut("UpdateConfernce/{id}")]
        public RequestResult UpdateConfernce(int id,[FromBody] ConferenceModel model){
            if(ModelState.IsValid){
                Conference tmp = dbContext.Conference.SingleOrDefault(x => x.ID == id);
                if(tmp != null){
                    tmp.Name = model.Name;
                    tmp.Timestamp = model.Timestamp;
                    dbContext.SaveChanges();
                    return RequestResult.GetRequestResult(false,tmp.ID + "");
                }else{
                    return RequestResult.ObjectNotFound();
                }
            }else{
                return RequestResult.ModelStateInvalid();
            }
        }

        [HttpDelete("DeleteConfernce/{id}")]
        public RequestResult DeleteConfernce(int id){
            if(ModelState.IsValid){
                Conference tmp = dbContext.Conference.SingleOrDefault(x => x.ID == id);
                if(tmp != null){
                    dbContext.Conference.Remove(tmp);
                    dbContext.SaveChanges();
                    return RequestResult.GetRequestResult(false,"Gelöscht");
                }else{
                    return RequestResult.ObjectNotFound();
                }
            }else{
                return RequestResult.ModelStateInvalid();
            }
        }
    }
}