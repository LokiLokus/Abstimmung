using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Abstimmung.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Abstimmung.RestAPI
{
    [Authorize]
    [Route("api/Vote")]
    public class VoteController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly DataContext dbContext;

        public VoteController(UserManager<User> userManager, SignInManager<User> signInManager, DataContext dbContext)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
        }

        [HttpGet("GetConference/{id}")]
        public async Task<ConferenceVoteModel> GetConferenceVotesAsync(int id)
        {
            User user = await userManager.FindByNameAsync(User.Identity.Name);
            Conference tmp = dbContext.Conference.SingleOrDefault(x => x.ID == id);
            if (tmp != null)
            {
                return ConferenceVoteModel.ToConferenceVoteModel(tmp, user.Id, dbContext);
            }
            return null;
        }

        [HttpPost("AddVoteToConference/{id}")]
        public RequestResult AddVoteToConference(int id, [FromBody] VoteModel vote)
        {
            if (ModelState.IsValid)
            {
                if (dbContext.Conference.SingleOrDefault(x => x.ID == id) != null)
                {
                    vote.Answers = vote.Answers.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    dbContext.Vote.Add(vote.ToVote(id));
                    dbContext.SaveChanges();
                    return RequestResult.GetRequestResult(false, "Frage hinzugefügt");
                }
                else
                {
                    return RequestResult.ObjectNotFound();
                }
            }
            else
            {
                return RequestResult.ModelStateInvalid();
            }
        }

        [HttpGet("GetStatistics/{id}")]
        public List<StatisticModel> GetStatistics(int id)
        {
            Conference tmp = dbContext.Conference.SingleOrDefault(x => x.ID == id);
            if (tmp != null)
            {
                //Get All Votes
                List<Vote> votes = dbContext.Vote.Where(x => x.ConferenceID == id).ToList();
                List<StatisticModel> result = new List<StatisticModel>();

                //Count for each Vote/Answer
                votes.ForEach(x =>
                {
                    List<Dictionary<string, int>> res = new List<Dictionary<string, int>>();
                    x.Answers.ForEach(y =>
                    {
                        var dic = new Dictionary<string, int>();
                        dic.Add(y, dbContext.UserVote.Where(z => z.VoteID == x.ID && z.Answer == y).Count());
                        res.Add(dic);
                    });
                    result.Add(
                    new StatisticModel()
                    {
                        Question = x.Question,
                        Answers = res
                    });
                });
                return result;

            }
            else
            {
                return null;
            }
        }

        [HttpPut("AnswerVote")]
        public async Task<RequestResult> AnswerVoteAsync([FromBody] ConferenceVoteModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await userManager.FindByNameAsync(User.Identity.Name);
                var votes = model.Votes.Where(x => !x.Answerd && x.SelectedAnswer != null).ToList();
                votes.ForEach(x =>
                {
                    dbContext.UserVote.Add(new UserVote()
                    {
                        Timestamp = DateTime.UtcNow,
                        VoteID = x.ID,
                        UserID = user.Id,
                        Answer = x.SelectedAnswer
                    });
                });
                await dbContext.SaveChangesAsync();
                return RequestResult.GetRequestResult(false, "Gespeichert");
            }
            else
            {
                return RequestResult.ModelStateInvalid();
            }
        }
    }
}