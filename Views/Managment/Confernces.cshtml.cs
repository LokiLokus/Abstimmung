using Abstimmung.DBModel;
using Abstimmung.DBModel.Model;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abstimmung.Controllers.Pages
{
    public class ConferenceModel : PageModel
    {
        private static DataContext dbContext = new DataContext();

        public static List<Conference> GetAllConfernces(){
            return dbContext.Conference.OrderBy(x => x.Timestamp)
                .Include(x => x.Creator).ToList();
        }

        public static List<Conference> GetNextConfernces(){
            return dbContext.Conference.Where(x => x.Timestamp >= DateTime.UtcNow.AddDays(-1)).OrderBy(x => x.Timestamp)
                .Include(x => x.Creator).ToList();
        }
    }
}